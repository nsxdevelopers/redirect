#Redirect

A minimal jonas app, packaged as a .war file.

##Variables
* warName: the final war package name
* contextRoot: the location where the war is deployed
* href: the redirect location